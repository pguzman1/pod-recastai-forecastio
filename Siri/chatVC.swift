//
//  chatVC.swift
//  Siri
//
//  Created by Patricio GUZMAN on 10/11/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

import JSQMessagesViewController
import RecastAI
import ForecastIO

struct User {
    let id: String
    let name: String
}
let conversationToken = "1234"

class chatVC: JSQMessagesViewController {
    let user1 = User(id: "1", name: "You")
    let user2 = User(id: "2", name: "Tim")
    var bot : RecastAIClient?
    var forecast: DarkSkyClient?
    
    var currentUser: User {
        return user1
    }
    
    var messages = [JSQMessage]()
}

extension chatVC {
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        
        messages.append(message!)
        self.sendText(text)
        finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        return NSAttributedString(string: messageUsername!)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        let message = messages[indexPath.row]
        if currentUser.id == message.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: .green)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: .blue)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
}

extension chatVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.senderId = currentUser.id
        self.senderDisplayName = currentUser.name
        self.messages = getMessages()
    }
}

extension chatVC {
    func getMessages() -> [JSQMessage] {
        var messages = [JSQMessage]()
        return messages
    }
    
    func sendText(_ message: String) {
        self.bot?.textConverse(message, converseToken: conversationToken, successHandler: {(response) in
            self.recastRequestDone(response)
        }, failureHandle: {(error) in
            print(error)
        })
    }
    
    func recastRequestDone(_ response : ConverseResponse) {
        if (response.replies?.count)! > 0 {
            print((response.replies?[0])!)
            messages.append(JSQMessage(senderId: "2", displayName: "bot", text: (response.replies?[0])!))
        }
        finishSendingMessage()
    }
}
