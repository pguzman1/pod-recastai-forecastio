//
//  ViewController.swift
//  Siri
//
//  Created by Patricio GUZMAN on 10/11/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit
import RecastAI
import ForecastIO

let RequestAccessToken = "a3446b8ab0ca18f6435ec6f0be984160"
let forecastToken = "fba942c3ee786a0cc8fe741647b9bf65"

class ViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var send: UIButton!
    var bot : RecastAIClient?
    var forecast: DarkSkyClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.label.text = ""
        self.bot = RecastAIClient(token : RequestAccessToken)
        forecast = DarkSkyClient(apiKey: forecastToken)
        forecast!.units = .si
        forecast!.language = .english
    }
    
    
    @IBAction func sendRequest(_ sender: UIButton) {
        if textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            
            self.bot?.textRequest(textField.text!, successHandler: {(response) in
                self.recastRequestDone(response)
            }, failureHandle: {(error) in
                print(error)
                self.label.text = "Error"
            })
        }
    }
    
    func recastRequestDone(_ response : Response)
    {
        print(response)
        if let intents = response.intents {
            if intents.count == 0 {
                label.text = "No intents"
            }
            else {
                label.text = intents[0].description
                if intents[0].description == "get-weather" {
                    self.getForecast(response)
                }
            }
        }
        else {
            label.text = "ERROR"
        }
    }
    
    func getForecast(_ response: Response) {
        if let location = response.get(entity: "location") {
            forecast?.getForecast(latitude: location["lat"] as! Double, longitude: location["lng"] as! Double) { result in
                switch result {
                case .success(let currentForecast, _):
                    let temperature = Int(currentForecast.currently?.temperature ?? 0)
                    DispatchQueue.main.async {
                        self.label.text = (currentForecast.currently?.summary)! + " \(temperature) °C"
                    }
                //  We got the current forecast!
                case .failure(let error):
                    print(error)
                    //  Uh-oh. We have an error!
                }
            }
        }
    }
    
    @IBAction func toChat(_ sender: UIButton) {
        performSegue(withIdentifier: "toChat", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chat = segue.destination as? chatVC {
            chat.bot = self.bot
            chat.forecast = self.forecast
        }
    }
}

